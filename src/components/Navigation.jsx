import { Link } from "react-router-dom";
import "./Navigation.css";
import { Outlet } from "react-router-dom"; //import Outlet

function Navigation() {
  return (
    <ul className="Navigation">
      {/* TODO add links to the different pages */}
      <li> <Link to={`/`} >Home</Link> </li>
      <li> <Link to={`/products`}>Products</Link></li>
      <li> <Link to={`/carts`}>Cart</Link></li>
      <div><Outlet/></div> {/* Afficher la navigation sur toute les pages avec Outlet*/}
    </ul>
  );
}

export default Navigation;
