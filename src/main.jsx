import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider, } from "react-router-dom";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";
import Navigation from "./components/Navigation";
import ErrorPage from "./components/error-page";

const router = createBrowserRouter([
  {
    path: "/", //racine qui reçoit Navigation 
    element: <Navigation />, 
    errorElement: <ErrorPage />, //création d'une page d'erreur
    children :[{ //déclaration des routes enfants
    path: "/", // rajout du composant home à la racine
    element: <Home />,
  },
  {
    path: "/carts",
    element: <Cart />,
  },
  {
    path: "/products",
    element: <ProductList />,
  },]
  },
  
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} /> {/* Appel de router*/}
  </React.StrictMode>
);
